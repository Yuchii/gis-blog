This project provides accompanying material discussed at
[GIS-Blog.com](http://www.gis-blog.com/)


Amongst others, it features topics related to remote sensing, web mapping,
geodata management,  extreme value analysis and environmental statistics.

Scripts can found in the [R](./R) and [Python](./Python) directories respectively.
Markdown files are collected in the [Documentation](./Documentation) directory.