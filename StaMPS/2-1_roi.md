# Table of Contents

* [Overview](./2_stamps_workflow.md)
* [Region of Interest](#1-get-region-of-interest)
* [Get SENTINEL-1 data](./2-2_get_data.md)
* [snap2stamps](./2-3_snap2stamps.md)
* [StaMPS](./2-4_StaMPS-steps.md)
* [StaMPS Visualizer](./2-5_shiny.md)

# 1 Get region of interest
A *GeoJSON* file (Simple Feature Access standard) describing the region of
interest is needed. The easiest way to create a ROI polygon is by converting
an existing Layer into a bounding box in QGIS
(Vector / Research Tools / Extract Layer Extent) or by creating a new layer
and manually mapping the ROI polygon:
 - *Layer / Create Layer / New Shapefile Layer*
 - *Toggle Editing*
 - *Edit / Add Feature*

Either way, results can be exported as GeoJSON via *Export / Save Features As / GeoJSON*.
Keep in mind to choose EPSG:4326 (WGS84) as CRS when exporting.

